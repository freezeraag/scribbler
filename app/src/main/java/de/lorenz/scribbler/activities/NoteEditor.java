package de.lorenz.scribbler.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import de.lorenz.scribbler.R;
import de.lorenz.scribbler.storage.Note;
import de.lorenz.scribbler.storage.NoteHelper;

/**
 * Created by Michael Lorenz
 * Created on 29.11.2016
 * <p>
 * This fragment class displays an editor for a note.
 * In this screen the three menu buttons are available.
 */

public class NoteEditor extends Fragment {

    private NoteHelper noteHelper;  // used to create a new note, update or delete a old note
    private Note note;              // the current note object, which is shown in the fragment
    private EditText content;       // field to edit the content of the note
    private String newContent;      // not yet saved content of the note

    public NoteEditor() {
    }

    /**
     * This method initializes the noteHelper and note variables.
     * The note-object is set to null, if no noteID was sent.
     *
     * @param noteHelper is used to save, update and delete notes
     * @param noteID     is used to get acces to the correct note to display
     */
    public void init(NoteHelper noteHelper, long noteID) {
        this.noteHelper = noteHelper;
        if (noteID != 0) {
            this.note = noteHelper.getNoteById(noteID);
        } else {
            note = null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.fragment_note_editor, container, false);

        content = (EditText) fragment.findViewById(R.id.textView_content);

        // if the note-object is null the user is about to create a new note
        if (note != null) {
            content.setText(note.getContent());
        }

        return fragment;
    }

    /**
     * This method is called, when a note's content should be updated in database, while the title stays the same.
     */
    public void updateContent() {
        note.setContent(newContent);
        noteHelper.updateNote(note);
    }

    /**
     * This method is called, when the fragment should be closed and the content has changed
     * or the save button was clicked.
     */
    public void saveContent(final boolean closeFragment) {
        this.newContent = content.getText().toString();

        // check if content is empty, if not save it with whitespaces later
        if (newContent.trim().isEmpty()) {
            // show message in snackbar, that empty messages are not saved
            Snackbar.make(getActivity().findViewById(R.id.content), R.string.snk_no_content_save, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            // check, if the note is a new one
            if (note == null) {

                // ask user to input a title for the new note
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.dlg_input_title);

                // set up an input field
                final EditText input = new EditText(getActivity());
                // specify the type of expected input
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                input.setText(R.string.dlg_new_note_name);

                // set up the buttons
                builder.setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newTitle = input.getText().toString().trim();

                        // check if title is empty
                        if (newTitle.isEmpty()) {
                            // show message in snackbar, that empty titles are not allowed
                            Snackbar.make(getActivity().findViewById(R.id.content), R.string.snk_title_empty, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        } else {
                            try {
                                // save note if title is valid
                                note = noteHelper.createNote(newTitle, newContent);
                            } catch (Exception e) {
                                // show message in snackbar, that title is already in use
                                Snackbar.make(getActivity().findViewById(R.id.content), R.string.snk_title_used, Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        }
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            } else {
                // check if content has changed
                if (!newContent.equals(note.getContent())) {
                    // open dialog with the options to discard changes or save them, if content has changed
                    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(getActivity());
                    deleteDialog.setMessage(R.string.dlg_saveNote);
                    deleteDialog.setCancelable(true);

                    // save the note, close the fragment and the dialog
                    deleteDialog.setPositiveButton(
                            R.string.btn_save,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    updateContent();
                                    closeFragment(closeFragment);
                                    dialog.cancel();
                                }
                            }
                    );

                    // discard changes, close the fragment and the dialog
                    deleteDialog.setNegativeButton(
                            R.string.btn_discard,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    closeFragment(true);
                                    dialog.cancel();
                                }
                            }
                    );

                    AlertDialog alert = deleteDialog.create();
                    alert.show();
                } else {
                    // close the fragment, if no changes in the note
                    closeFragment(closeFragment);
                }
            }
        }
    }

    /**
     * This method is called, when the delete button is clicked.
     */
    public void deleteContent() {
        if (note != null) {
            try {
                noteHelper.deleteNote(note);
            } catch (Exception e) {
                // show user in snackbar that the note could not be deleted
                Snackbar.make(getActivity().findViewById(R.id.content), R.string.snk_not_delete_note, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
        ((MainScreen) getActivity()).setRoot(true);
        closeFragment(true);
    }

    /**
     * This method sends an intent for rfc822 capable (e.g. email) apps.
     * EMail subject        note title
     * EMail content        note content
     */
    public void shareNote() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.share_email_example)});

        // if the note is a new one the user have to enter a email subject, else the note title is used
        String title;
        if (note == null) {
            title = getString(R.string.share_title_example);
        } else {
            title = note.getTitle();
        }
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, title);

        // put the current content as email content,
        // if there is no content the user gets the information that no message could be sent
        String mailText = content.getText().toString();
        if (mailText.trim().isEmpty()) {
            Snackbar.make(getActivity().findViewById(R.id.content), R.string.snk_no_content_share, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        } else {
            emailIntent.putExtra(Intent.EXTRA_TEXT, mailText);
            try {
                startActivity(Intent.createChooser(emailIntent, getString(R.string.share_send_mail)));
            } catch (android.content.ActivityNotFoundException ex) {
                Snackbar.make(getActivity().findViewById(R.id.content), R.string.share_no_client, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

    }

    /**
     * A method to close the fragment by itself.
     */
    public void closeFragment(boolean closeFragment) {
        if (closeFragment) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.popBackStack();
        }
    }
}
