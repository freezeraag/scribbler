package de.lorenz.scribbler.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import de.lorenz.scribbler.R;
import de.lorenz.scribbler.adapter.NoteAdapter;
import de.lorenz.scribbler.storage.Note;
import de.lorenz.scribbler.storage.NoteHelper;

/**
 * Created by Michael Lorenz
 * Created on 29.11.2016
 * <p>
 * This class represents the logic of the main menu.
 * In here the recyclerView gets int's content, the items get their click and longClick listeners.
 */

public class MainScreen extends AppCompatActivity {

    private boolean root = true;            // shows the activity's state
    private NoteHelper noteHelper;          // access to save, update or delete notes in the database
    private NoteEditor noteEditor;          // current noteEditor, if displayed
    private FloatingActionButton addNote;   // button to add new notes in menu screen
    private MenuItem deleteBtn;             // menu item for deleting a note shown in editor-mode
    private MenuItem saveBtn;               // menu item for saving a note shown in editor-mode
    private MenuItem settingsBtn;           // menu item for sharing a note shown in editor-mode
    NoteAdapter noteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /**
         * Logic for the floating action button
         * When clicked the noteEditor starts with empty content and title.
         */
        addNote = (FloatingActionButton) findViewById(R.id.addNoteBtn);
        if (addNote != null) {
            addNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // change content-view to noteEditor
                    FragmentManager fm = getSupportFragmentManager();
                    noteEditor = new NoteEditor();
                    noteEditor.init(noteHelper, 0);
                    fm.beginTransaction()
                            .replace(R.id.content, noteEditor)
                            .addToBackStack(null)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commitAllowingStateLoss();
                    setRoot(false);
                }
            });
        }
    }

    /**
     * This method inflates the menu_bar_layout.xml to get access to the buttons.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bar_layout, menu);

        deleteBtn = menu.findItem(R.id.menu_delete);
        saveBtn = menu.findItem(R.id.menu_save);
        settingsBtn = menu.findItem(R.id.menu_share);

        setRoot(root);
        return true;
    }

    /**
     * This method performs actions, depending on the clicked menu button.
     * <p>
     * menu_delete      delete current note in editor
     * menu_save        try to save current state of the note
     * menu_share       send intent for rfc822 capable (e.g. email) apps
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_delete:
                noteEditor.deleteContent();
                break;
            case R.id.menu_save:
                noteEditor.saveContent(false);
                break;
            case R.id.menu_share:
                noteEditor.shareNote();
                break;
        }
        return true;
    }

    /**
     * This method enables/disables the menu buttons.
     *
     * @param activate a boolean to switch states
     */
    public void toggleMenuButtons(boolean activate) {
        deleteBtn.setEnabled(activate).setVisible(activate);
        saveBtn.setEnabled(activate).setVisible(activate);
        settingsBtn.setEnabled(activate).setVisible(activate);

        // make addButton visible in menu, but not in editor
        if (activate) {
            addNote.setVisibility(View.GONE);
        } else {
            addNote.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Catch the back button soft key.
     * When in menu, close the app.
     * When in noteEditor, try to save the note.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && !isRoot()) {
            noteEditor.saveContent(true);
            setRoot(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean isRoot() {
        return root;
    }

    /**
     * @param root a boolean set to true when in menu and false when in noteEditor.
     */
    public void setRoot(boolean root) {
        this.root = root;
        if (root) {
            loadNotes();
        }
        toggleMenuButtons(!root);
    }

    /**
     * This method is used to load all notes from the database whenever the menu is shown.
     * In here the single noteItems from recyclerView are getting their click and longClick listeners.
     */
    private void loadNotes() {
        noteHelper = new NoteHelper(this);
        final ArrayList<Note> notes = noteHelper.getNotes();

        // clickListener implementation, when a note is clicked in menu
        NoteAdapter.OnNoteClickListener listener = new NoteAdapter.OnNoteClickListener() {
            @Override
            public void onNoteClick(Note note) {
                // change the content-view to noteEditor with the given note on click
                int noteID = note.getTitle().hashCode();

                // change content-view to noteEditor
                FragmentManager fm = getSupportFragmentManager();
                noteEditor = new NoteEditor();
                noteEditor.init(noteHelper, noteID);
                fm.beginTransaction()
                        .replace(R.id.content, noteEditor, note.getTitle())
                        .addToBackStack(null)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commitAllowingStateLoss();
                setRoot(false);

                // show title of the selected note in snackbar
                RelativeLayout content = (RelativeLayout) findViewById(R.id.content);
                if (content != null) {
                    Snackbar.make(content, note.getTitle(), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        };

        // longClickListener implementation, when a note is clicked long in menu to open up a context menu
        NoteAdapter.OnNoteLongClickListener longListener = new NoteAdapter.OnNoteLongClickListener() {
            @Override
            public void onNoteLongClick(final Note selectedNote, final Context context) {
                // building a dialog with the options for the selected note
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                // giving the dialog a title
                builder.setTitle(selectedNote.getTitle());

                // put in two dialog options to choose from
                final String[] menuItems = {context.getString(R.string.rename), context.getString(R.string.delete)};

                builder.setItems(menuItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        // option handling whether rename or delete is chosen
                        // rename the selected note
                        if (menuItems[item].equals(getString(R.string.rename))) {

                            // ask user to input a new title for the note
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(R.string.dlg_input_title);

                            // set up an input field
                            final EditText input = new EditText(context);
                            // specify the type of expected input
                            input.setInputType(InputType.TYPE_CLASS_TEXT);
                            builder.setView(input);

                            // put current title in the input field
                            input.setText(selectedNote.getTitle());

                            // set up the buttons
                            builder.setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    RelativeLayout content = (RelativeLayout) findViewById(R.id.content);
                                    String newTitle = input.getText().toString().trim();

                                    // check if title is empty
                                    if (newTitle.isEmpty()) {
                                        // show message in snackbar, that empty titles are not allowed
                                        if (content != null) {
                                            Snackbar.make(content, R.string.snk_title_empty, Snackbar.LENGTH_LONG)
                                                    .setAction("Action", null).show();
                                        }
                                    } else {
                                        // rename note if title is valid
                                        Note newNote;
                                        try {
                                            newNote = noteHelper.renameNote(selectedNote, newTitle);

                                            if (content != null) {
                                                // show message in snackbar that oldTitle renamed to newTitles
                                                String infoText = selectedNote.getTitle() + " "
                                                        + getString(R.string.snk_context_result_renamed) + " " + newNote.getTitle();
                                                Snackbar.make(content, infoText, Snackbar.LENGTH_LONG)
                                                        .setAction("Action", null).show();
                                                // tell the adapter that an item was removed and a new created
                                                noteAdapter.removeItem(selectedNote);
                                                noteAdapter.addItem(newNote);
                                            }
                                        } catch (Exception e) {
                                            // show message in snackbar, that title is already in use
                                            if (content != null) {
                                                Snackbar.make(content, R.string.snk_title_used, Snackbar.LENGTH_LONG)
                                                        .setAction("Action", null).show();
                                            }
                                        }
                                    }
                                    dialog.cancel();
                                }
                            });
                            builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();

                            // delete the selected note
                        } else if (menuItems[item].equals(getString(R.string.delete))) {
                            try {
                                noteHelper.deleteNote(selectedNote);
                            } catch (Exception e) {
                                // show user in snackbar that the note could not be deleted
                                RelativeLayout content = (RelativeLayout) findViewById(R.id.content);
                                if (content != null) {
                                    Snackbar.make(content, R.string.snk_not_delete_note, Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                }
                            }

                            // tell the adapter that an item was removed
                            noteAdapter.removeItem(selectedNote);

                            // display the result of the selected option
                            RelativeLayout content = (RelativeLayout) findViewById(R.id.content);
                            if (content != null) {
                                Snackbar.make(content, selectedNote.getTitle() + " " + getString(R.string.snk_context_result_deleted), Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        }

                    }
                });
                builder.show();
            }
        };

        // attach the two listeners to the noteAdapter
        noteAdapter = new NoteAdapter(notes, listener, longListener);

        RecyclerView notesRecyclerView = (RecyclerView) findViewById(R.id.noteList);

        if (notesRecyclerView != null) {
            // attach adapter to recyclerView
            notesRecyclerView.setAdapter(noteAdapter);

            // define layout for recyclerview
            notesRecyclerView.setLayoutManager(new GridLayoutManager(this,
                    getResources().getInteger(R.integer.recycler_max_columns)));

            // set an remove animation to the recyclerView with a defaultItemAnimator
            DefaultItemAnimator animator = new DefaultItemAnimator();
            animator.setRemoveDuration(getResources().getInteger(R.integer.recycler_max_columns));
            notesRecyclerView.setItemAnimator(animator);
        }
    }
}