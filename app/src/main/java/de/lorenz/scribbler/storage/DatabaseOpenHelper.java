package de.lorenz.scribbler.storage;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import de.lorenz.scribbler.R;

/**
 * Created by Michael Lorenz
 * Created on 29.11.2016
 * <p>
 * This class creates a database, if instatiated for the first time.
 * Further starts of the application cause changes to the database as soon as the version number changes.
 * Changes to the database should be made using the onUpdate method.
 * Call sequence:
 * - constructor
 * - onCreate
 * - onUpdate
 */

class DatabaseOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "scribbler_db";
    private static final int DATABASE_VERSION = 1;
    private Resources r;

    DatabaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        // application resources
        this.r = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /**
         * This table contains tuples of notes with title, content and createdOn/editedOn date.
         * The title acts as ID.
         * To avoid typos within the application, I decided to save the names for tables and their rows
         * in the strings.xml file.
         */
        db.execSQL("CREATE TABLE " + r.getString(R.string.notes_table_name) + "(" +
                r.getStringArray(R.array.notes_table_columns)[0] + " TEXT PRIMARY KEY," +
                r.getStringArray(R.array.notes_table_columns)[1] + " TEXT," +
                r.getStringArray(R.array.notes_table_columns)[2] + " DATETIME);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /** example for changes for the next version of the database
         *
         * if (oldVersion < 2) {
         *
         * }
         */
    }
}
