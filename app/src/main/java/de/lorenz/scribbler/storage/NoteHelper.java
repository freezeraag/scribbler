package de.lorenz.scribbler.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.LongSparseArray;

import java.util.ArrayList;
import java.util.Calendar;

import de.lorenz.scribbler.R;

/**
 * Created by Michael Lorenz
 * Created on 29.11.2016
 * <p>
 * This class collects all notes that were saved in the database and provides it to the application.
 * Furthermore, the notes are created, edited or deleted in the database.
 */

public class NoteHelper {

    private SQLiteDatabase db;              // used to get connection to the database
    private String tableNameNotes;          // contains the name of the table for notes in the database
    private String[] tableNotesColumns;     // contains the names of the columns for notes the notes table
    private LongSparseArray<Note> notes;    // contains references from note id to the note-object

    public NoteHelper(Context context) {
        DatabaseOpenHelper dbOpenHelper = new DatabaseOpenHelper(context);
        db = dbOpenHelper.getReadableDatabase();
        tableNameNotes = context.getResources().getString(R.string.notes_table_name);
        tableNotesColumns = context.getResources().getStringArray(R.array.notes_table_columns);
        notes = new LongSparseArray<>();
        refreshNoteLibrary();
    }

    /**
     * This method returns the requested note from the HashMap by id.
     * The key value is the id of the note equal to the hashcode of the title and
     * the value is the note object.
     *
     * @return the requested Note by id.
     */
    public Note getNoteById(Long id) {
        return notes.get(id);
    }

    /**
     * This method lets you save notes.
     *
     * @return the created note, if the values could be saved correctly.
     * note object      if saved correctly
     * Exception        else
     */
    public Note createNote(String title, String content) throws Exception {
        ContentValues cv = new ContentValues();
        cv.put(tableNotesColumns[0], title);
        cv.put(tableNotesColumns[1], content);
        cv.put(tableNotesColumns[2], getDateString());

        // insert the new note into the database with rollback option, if exception
        try {
            db.insertWithOnConflict(tableNameNotes, null, cv, SQLiteDatabase.CONFLICT_ROLLBACK);
            refreshNoteLibrary();
            return getNoteById((long) title.hashCode());
        } catch (Exception e) {
            throw new Exception("Name already assigned.");
        }
    }

    /**
     * This method uses the previous implemented methods createNote and deleteNote to rename a note.
     *
     * @param oldNote  note which should be renamed
     * @param newTitle new title for the note
     * @return the created note, if the title could be saved correctly.
     * note object      if saved correctly or title did not changed
     * Exception        else
     */
    public Note renameNote(Note oldNote, String newTitle) throws Exception {
        // return oldNote if new and old title equals
        if (oldNote.getTitle().equals(newTitle)) {
            return oldNote;
        }
        Note newNote = createNote(newTitle, oldNote.getContent());

        deleteNote(oldNote);
        return newNote;
    }

    /**
     * This method lets you update/replace notes and ignore unique titles.
     * This method is called, when the title of an edited note has not changed.
     *
     * @return the updated note object, if the values could be saved correctly.
     * note object      if updated correctly
     * old note object  if errors occurs
     */
    public Note updateNote(Note note) {
        ContentValues cv = new ContentValues();
        cv.put(tableNotesColumns[0], note.getTitle());
        cv.put(tableNotesColumns[1], note.getContent());
        cv.put(tableNotesColumns[2], getDateString());

        long returnValue = db.insertWithOnConflict(tableNameNotes, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        if (returnValue > 0) {
            refreshNoteLibrary();
            return getNoteById((long) note.getTitle().hashCode());
        } else {
            return note;
        }
    }

    /**
     * This method lets you delete an existing note.
     *
     * @return a long value if a note could be deleted correctly or not.
     * n            number of rows affected
     * Exception    if no row deleted
     */
    public long deleteNote(Note note) throws Exception {
        long returnValue = db.delete(tableNameNotes, tableNotesColumns[0] + "=?", new String[]{note.getTitle()});

        if (returnValue != 0) {
            refreshNoteLibrary();
            return returnValue;
        } else {
            throw new Exception("Note not deleted.");
        }
    }

    /**
     * This method refreshes the HashMap based on the database current content.
     */
    private void refreshNoteLibrary() {
        // cursor for navigating through the database based on the given table
        Cursor cursor = db.query(tableNameNotes, tableNotesColumns,
                null, null, null, null, tableNotesColumns[0] + " ASC", null);

        notes.clear();

        while (cursor.moveToNext()) {
            String title = cursor.getString(0);
            Note note = new Note(title, cursor.getString(1), cursor.getString(2));

            // put the note into the HashMap
            notes.put((long) note.getTitle().hashCode(), note);
        }
        cursor.close();
    }

    /**
     * This method returns all notes that were saved in the database.
     *
     * @return an ArrayList with all notes
     */
    public ArrayList<Note> getNotes() {
        ArrayList<Note> noteArray = new ArrayList<>();

        for (int index = 0; index < notes.size(); index++) {
            noteArray.add(notes.valueAt(index));
        }
        return noteArray;
    }

    private String getDateString() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        return cal.get(Calendar.DAY_OF_MONTH) + "/" +
                cal.get(Calendar.MONTH) + "/" +
                cal.get(Calendar.YEAR);
    }
}
