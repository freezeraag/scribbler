package de.lorenz.scribbler.storage;

/**
 * Created by Michael Lorenz
 * Created on 29.11.2016
 *
 * Implementation of the note class.
 */

public class Note {

    private String title;
    private String content;
    private String date;

    Note(String title, String content, String date) {
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }
}
