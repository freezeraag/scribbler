package de.lorenz.scribbler.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import de.lorenz.scribbler.R;
import de.lorenz.scribbler.storage.Note;

/**
 * Created by Michael Lorenz
 * Created on 01.12.2016.
 * <p>
 * This adapter class is used to define how connected ArrayList<Note> should be displayed in the connected recyclerView.
 * Additionally a click and longClick listener is bound to it, so that the app can
 * react to clicks on items from the recyclerView.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

    // interface for a clickListener
    public interface OnNoteClickListener {
        void onNoteClick(Note note);
    }

    // interface for a longClickListener
    public interface OnNoteLongClickListener {
        void onNoteLongClick(Note note, Context context);
    }

    private final OnNoteClickListener listener;
    private final OnNoteLongClickListener longListener;
    private ArrayList<Note> noteArrayList;

    public NoteAdapter(ArrayList<Note> noteArrayList, OnNoteClickListener listener, OnNoteLongClickListener longListener) {
        this.listener = listener;
        this.longListener = longListener;
        this.noteArrayList = noteArrayList;
    }

    // This method tells the observer that the list has changed because item added
    public void addItem(Note note) {
        noteArrayList.add(note);
        notifyItemInserted(noteArrayList.size());
    }

    // This method tells the observer that the list has changed because item removed
    public void removeItem(Note note) {
        int position = noteArrayList.indexOf(note);
        if (position != -1) {
            noteArrayList.remove(note);
            notifyItemRemoved(position);
        }
    }

    // This method creates the viewHolder and hands over the layout of a single item.
    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_layout, parent, false);
        return new NoteViewHolder(view);
    }

    // This method provides the data, that should be displayed to the viewHolder.
    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        Note note = noteArrayList.get(position);
        holder.textViewTitle.setText(note.getTitle());
        holder.textViewDate.setText(note.getDate());
        holder.bindListeners(note, listener, longListener);

    }

    // This method returns the amount of items that should be displayed in recyclerView
    @Override
    public int getItemCount() {
        if (noteArrayList.size() > 0) {
            return noteArrayList.size();
        } else {
            return 0;
        }
    }

    // The viewHolder class shows, where data from a class should be displayed in a custom layout.
// In addition it binds the listeners.
    static class NoteViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView textViewTitle;
        TextView textViewDate;

        NoteViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.noteCard);
            textViewTitle = (TextView) itemView.findViewById(R.id.textView_note_title);
            textViewDate = (TextView) itemView.findViewById(R.id.textView_note_date);
        }

        void bindListeners(final Note note, final OnNoteClickListener listener, final OnNoteLongClickListener longListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onNoteClick(note);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    longListener.onNoteLongClick(note, view.getContext());
                    return true;
                }
            });
        }
    }
}
