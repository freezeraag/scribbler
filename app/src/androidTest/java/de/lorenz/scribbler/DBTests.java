package de.lorenz.scribbler;

import android.test.InstrumentationTestCase;

import de.lorenz.scribbler.storage.Note;
import de.lorenz.scribbler.storage.NoteHelper;

/**
 * Created by Michael Lorenz
 * Created on 02.12.2016
 * <p>
 * This class is for unit testing database saving and deleting operations.
 */

public class DBTests extends InstrumentationTestCase {

    private NoteHelper noteHelper;
    private String title;
    private String content;
    private Note note;
    private boolean noteSavePassed;
    private boolean noteDeletePassed;

    // This method runs before each test run
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    // this test shows, if the a note can be saved in the database correctly
    public void test_01_SaveValidNote() {
        givenDataForInitializeNote();
        whenNoteIsSaved();
        thenVerifyNoSaveException();
    }

    private void givenDataForInitializeNote() {
        noteHelper = new NoteHelper(getInstrumentation().getTargetContext());
        title = "Test-Titel";
        content = "Test-Inhalt";
    }

    private void whenNoteIsSaved() {
        try {
            note = noteHelper.createNote(title, content);
            noteSavePassed = true;
        } catch (Exception e) {
            e.printStackTrace();
            noteSavePassed = false;
        }
    }

    private void thenVerifyNoSaveException() {
        assertTrue(noteSavePassed);
    }

    // this test shows, if the a note can be deleted from the database correctly
    public void test_02_DeleteValidNote() {
        givenDataForInitializeNote();
        whenNoteIsDeleted();
        thenVerifyNoDeleteException();
    }

    private void whenNoteIsDeleted() {
        try {
            noteHelper.deleteNote(noteHelper.getNoteById((long) title.hashCode()));
            noteDeletePassed = true;
        } catch (Exception e) {
            e.printStackTrace();
            noteDeletePassed = false;
        }
    }

    private void thenVerifyNoDeleteException() {
        assertTrue(noteDeletePassed);
    }

    // This method runs after each test run
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
